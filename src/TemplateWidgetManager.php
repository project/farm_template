<?php

namespace Drupal\farm_template;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\farm_template\Attribute\TemplateWidget;
use Drupal\farm_template\Plugin\Template\Widget\TemplateWidgetInterface;

/**
 * Plugin manager for template widget plugins.
 */
class TemplateWidgetManager extends DefaultPluginManager implements TemplateWidgetManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function getFieldWidgetOptions(FieldDefinitionInterface $field_definition) {
    $options = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      if (($instance = $this->createInstance($id)) && $instance->supportsFieldDefinition($field_definition)) {
        $options[$id] = $definition['label'];
      }
    }
    return $options;
  }

  /**
   * Constructs a TemplateManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Template/Widget',
      $namespaces,
      $module_handler,
      TemplateWidgetInterface::class,
      TemplateWidget::class,
    );
    $this->alterInfo('farm_template_widget_info');
    $this->setCacheBackend($cache_backend, 'farm_template_widget');
  }

}
