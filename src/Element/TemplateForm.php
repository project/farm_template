<?php

namespace Drupal\farm_template\Element;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a farm template form element.
 *
 * @FormElement("farm_template")
 */
class TemplateForm extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => FALSE,
      '#process' => [
        [$class, 'processGroup'],
        [$class, 'processTemplate'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
      ],
      '#element_validate' => [
        [$class, 'validateTemplate'],
      ],
      '#process_default_value' => TRUE,
      '#theme_wrappers' => ['form_element'],
      '#overrides' => [],
      '#debug' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {

    // Templates do not support modifying an existing entity.
    // Simply return if a default value is provided.
    // @todo Support pre-populating templates with existing entities.
    if (isset($element['#default_value'])) {
      return $element['#default_value'];
    }

    // The entity value is set in the validate callback.
    return $element['#value'] ?? [];
  }

  /**
   * Adds entity autocomplete functionality to a form element.
   *
   * @param array $element
   *   The form element to process. Properties used:
   *   - #target_type: The ID of the target entity type.
   *   - #selection_handler: The plugin ID of the entity reference selection
   *     handler.
   *   - #selection_settings: An array of settings that will be passed to the
   *     selection handler.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The form element.
   *
   * @throws \InvalidArgumentException
   *   Exception thrown when the #target_type or #autocreate['bundle'] are
   *   missing.
   */
  public static function processTemplate(array &$element, FormStateInterface $form_state, array &$complete_form) {

    // Build the template into the form.
    // Nothing to do if there is no target entity type.
    if (empty($element['#template'])) {
      throw new \InvalidArgumentException('Missing required #template parameter.');
    }

    // If a default value is provided, render the entity but do nothing more.
    if (isset($element['#default_value'])) {
      $element['target_id'] = [
        '#type' => 'hidden',
        '#value' => $element['#default_value']->id(),
      ];

      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $element['#default_value'];
      $element['text'] = [
        '#markup' => "{$entity->getEntityType()->getLabel()}: {$entity->toLink($entity->label())->toString()}",
      ];
      return $element;
    }

    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $template = $element['#template'];

    // Create default entity. This is needed for building field widgets.
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_storage = $entity_type_manager->getStorage($template->getTargetEntityTypeId());
    $default_entity = $entity_storage->create(['type' => $template->getTargetBundle()]);

    // Widget defaults. TODO.
    $widget_defaults = [
      'entity_autocomplete' => [
        '#target_type' => 'asset',
      ],
    ];

    /** @var \Drupal\farm_template\TemplateWidgetManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.farm_template_widget');

    // Add each template field to the form.
    $element['#tree'] = TRUE;
    $overrides = $element['#overrides'] ?? [];
    $template_fields = $template->get('fields') ?? [];
    foreach ($template_fields as $template_field) {

      // Include field overrides if provided.
      $field_id = $template_field['id'];
      if (isset($overrides[$field_id])) {
        $template_field['required'] = $overrides[$field_id]['required'] ?? FALSE;
        $template_field['hidden'] = $overrides[$field_id]['hidden'] ?? FALSE;
        $template_field['widget_settings']['default_value'] = $overrides[$field_id]['default_value'] ?? NULL;
      }

      // Allow the plugin type to provide a subform.
      $field_definitions = $template->fieldDefinitions();
      $field_definition = $field_definitions[$template_field['field_name']];
      /** @var \Drupal\farm_template\Plugin\Template\Widget\TemplateWidgetInterface $plugin_instance */
      $plugin_instance = $manager->createInstance($template_field['widget'], $template_field);
      $element[$field_id] = $plugin_instance->render($template_field, $field_definition, $default_entity);

      // Handle hidden and debug states.
      if ($template_field['hidden'] == TRUE) {
        if ($element['#debug'] === TRUE) {
          $element[$field_id]['#disabled'] = TRUE;
        }
        else {
          $element[$field_id]['#type'] = 'hidden';
        }
      }
    }

    return $element;
  }

  /**
   * Form element validation handler for template elements.
   */
  public static function validateTemplate(array &$element, FormStateInterface $form_state, array &$complete_form) {

    // Entity value that we will set to form state.
    $entity = NULL;

    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $template = $element['#template'];

    // If there is input data, process it and create a new entity.
    if (isset($element['#value']) && is_array($element['#value'])) {

      // Start an array of entity data.
      // @todo Do not hard-code 'type' as bundle field name.
      $entity_data = [
        'type' => $template->getTargetBundle(),
        'template' => $template,
      ];

      // Iterate over field definitions from the template.
      $field_definitions = $template->fieldDefinitions();
      $template_fields = $template->get('fields') ?? [];
      foreach ($template_fields as $template_field) {

        // Determine the field value to set for the entity.
        $field_value = NULL;

        $form_state_key = [...$element['#parents'], $template_field['id']];

        // If the field is hidden, use the fields default value.
        // @todo Process tokens for more complex replacements.
        if ($template_field['hidden'] == TRUE) {
          $field_value = $template_field['widget_settings']['default_value'] ?? NULL;
        }
        elseif ($form_state->hasValue($form_state_key)) {
          $field_value = $form_state->getValue($form_state_key);
        }

        // Add edge case for timestamp fields/datetime widgets.
        // @todo Should replace and allow widgets to process the submit.
        // This is basically why Form Widgets have a massage form values.
        // @see TimestampDatetimeWidget.
        if ($field_value instanceof DrupalDateTime) {
          $field_value = $field_value->getTimestamp();
        }

        // Skip if we still don't have a field value.
        if ($field_value === NULL) {
          continue;
        }

        // Add the field value to entity data.
        $field_name = $template_field['field_name'];
        /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
        $field_definition = $field_definitions[$field_name];

        // Handle fields that allow multiple values.
        $is_multiple = $field_definition->getFieldStorageDefinition()->isMultiple();

        // Create if does not already exist. Start an array for the field item.
        // This works regardless of cardinality because field definitions are
        // all list data definitions.
        // @see Drupal\Core\Field\FieldDefinition.
        $field_value = is_array($field_value) ? $field_value : [$field_value];
        if (!isset($entity_data[$field_name])) {
          $entity_data[$field_name] = $field_value;
        }

        // Combine values when multiple template fields exist for the same
        // entity field.
        // If multiple, simply add to the existing array.
        elseif ($is_multiple) {
          array_push($entity_data[$field_name], ...$field_value);
        }
        // If not multiple, use logic based on field type.
        else {
          // @todo For now we just append.
          foreach ($field_value as $value) {
            $entity_data[$field_name][0] .= $value;
          }
        }
      }

      // Create entity.
      $entity_type_manager = \Drupal::entityTypeManager();
      $entity_storage = $entity_type_manager->getStorage($template->getTargetEntityTypeId());
      $entity = $entity_storage->create($entity_data);

      // Check for violations.
      $violations = $entity->validate();
      if (count($violations)) {
        foreach ($violations as $violation) {

          // Check if this is a nested path.
          $path = $violation->getPropertyPath();
          $parts = explode('.', $path);
          if (count($parts)) {

            // Get the mapped field names responsible for the validation error.
            // It is possible that the template field has multiple form fields
            // related to a single violation.
            $target_part = reset($parts);
            $violation_fields = $template->getTemplateEntityFields($target_part);
            if (count($violation_fields)) {
              foreach ($violation_fields as $field_id => $violation_field) {
                if ($violation_field['hidden'] ?? FALSE) {
                  $label = $violation_field['label'] ?? $field_id;
                  $form_state->setError($element[$field_id], "$label: {$violation->getMessage()}");
                }
                $form_state->setError($element[$field_id], $violation->getMessage());
              }
            }
            // Else handle if there is a violation for a field that is not
            // in the template. Try to use the field definition if possible.
            else {
              $field_label = $target_part;
              if (($invalid_part = $violation->getInvalidValue()) && $invalid_part instanceof FieldItemListInterface) {
                $field_label = $invalid_part->getFieldDefinition()->getLabel();
              }
              $message = new TranslatableMarkup('Validation error for field %field: @message', ['%field' => $field_label, '@message' => $violation->getMessage()]);
              $form_state->setError($element, $message);
            }

          }
        }
        return;
      }
    }

    // Finally, set the element value to the new entity.
    $form_state->setValueForElement($element, $entity);
  }

}
