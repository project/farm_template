<?php

namespace Drupal\farm_template\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\farm_template\TemplateWidgetPluginCollection;

/**
 * Entity template configuration entity.
 *
 * @ConfigEntityType(
 *   id = "farm_record_template",
 *   label = @Translation("Record Template"),
 *   handlers = {
 *     "access" = "\Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add" = "Drupal\farm_template\Form\RecordTemplateForm",
 *       "edit" = "Drupal\farm_template\Form\RecordTemplateForm",
 *       "fields" = "Drupal\farm_template\Form\RecordTemplateFieldsForm",
 *       "field" = "Drupal\farm_template\Form\RecordTemplateFieldForm",
 *       "add_field" = "Drupal\farm_template\Form\RecordTemplateFieldForm",
 *       "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\farm_template\RecordTemplateListBuilder",
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *   },
 *   admin_permission = "administer farm_record_template",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/farm/setup/template/{farm_record_template}/view",
 *     "add-form" = "/farm/setup/template/add",
 *     "edit-form" = "/farm/setup/template/{farm_record_template}/edit",
 *     "fields-form" = "/farm/setup/template/{farm_record_template}/fields",
 *     "delete-form" = "/farm/setup/template/{farm_record_template}/delete",
 *     "collection" = "/farm/setup/template"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "targetEntityType",
 *     "bundle",
 *     "fields",
 *   }
 * )
 */
class RecordTemplate extends ConfigEntityBase implements ConfigEntityInterface, EntityDescriptionInterface, EntityWithPluginCollectionInterface {

  /**
   * Unique ID for the config entity.
   *
   * @var string
   */
  protected $id;

  /**
   * The template label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this template.
   *
   * @var string
   */
  protected $description;

  /**
   * Entity type to be displayed.
   *
   * @var string
   */
  protected $targetEntityType;

  /**
   * Bundle to be displayed.
   *
   * @var string
   */
  protected $bundle;

  /**
   * Whether this display is enabled or not.
   *
   * If the entity (form) display is disabled, we'll fall back to the 'default'
   * display.
   *
   * @var bool
   */
  protected $status;

  /**
   * List of field display options, keyed by component name.
   *
   * @var array
   */
  protected $fields = [];

  /**
   * A list of field definitions eligible for configuration in this display.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $fieldDefinitions;

  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\farm_template\TemplateWidgetPluginCollection
   */
  protected $pluginCollection;

  /**
   * Encapsulates the creation of the plugin collection.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   *   The template widget plugin collection.
   */
  protected function getPluginCollection() {
    if (!$this->pluginCollection) {
      $this->pluginCollection = new TemplateWidgetPluginCollection(\Drupal::service('plugin.manager.farm_template_widget'), $this->fields);
    }
    return $this->pluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'fields' => $this->getPluginCollection(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->targetEntityType;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle() {
    return $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetBundle($bundle) {
    $this->set('bundle', $bundle);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    return $this->set('description', $description);
  }

  /**
   * Helper function to return a record type label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   The label string.
   */
  public function getRecordTypeLabel() {
    if (!$this->targetEntityType && !$this->bundle) {
      return "";
    }

    $entity_type = \Drupal::entityTypeManager()->getDefinition($this->targetEntityType);
    $bundle_type = $entity_type->getBundleEntityType();
    if ($bundle = \Drupal::entityTypeManager()->getStorage($bundle_type)->load($this->bundle)) {
      return "{$bundle->label()} {$entity_type->getSingularLabel()}";
    }

    return $entity_type->getSingularLabel();
  }

  /**
   * Retrieves the field definitions for the entity type and bundle.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions for the entity type and bundle.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the target entity type or bundle is empty.
   */
  public function fieldDefinitions() {

    // Entity type and bundle are required.
    if (empty($this->targetEntityType) || empty($this->bundle)) {
      return [];
    }

    // Return cached values.
    if (isset($this->fieldDefinitions)) {
      return $this->fieldDefinitions;
    }

    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($this->targetEntityType, $this->bundle);
    $this->fieldDefinitions = array_filter($field_definitions, function (FieldDefinitionInterface $field_definition) {
      return !$field_definition->isComputed() && $field_definition->isDisplayConfigurable('form');
    });

    uasort($this->fieldDefinitions, function (FieldDefinitionInterface $field_one, FieldDefinitionInterface $field_two) {
      $default_weight = 0;
      return ($field_one->getDisplayOptions('form')['weight'] ?? $default_weight) -
        ($field_two->getDisplayOptions('form')['weight'] ?? $default_weight);
    });

    return $this->fieldDefinitions;
  }

  /**
   * Get the template entity fields by field name.
   *
   * @param string $field_name
   *   The name of the field.
   *
   * @return array
   *   The template entity fields filtered by field name.
   */
  public function getTemplateEntityFields(string $field_name) {
    $fields = $this->get('fields') ?? [];
    $fields = array_filter($fields, function (array $field) use ($field_name) {
      return $field['field_name'] == $field_name;
    });

    $field_ids = array_column($fields, 'id');
    return array_combine($field_ids, $fields);
  }

}
