<?php

namespace Drupal\farm_template;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

class TemplateWidgetPluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  protected $pluginKey = 'widget';

}
