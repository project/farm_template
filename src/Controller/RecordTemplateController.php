<?php

namespace Drupal\farm_template\Controller;

use Drupal\asset\Entity\AssetType;
use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\log\Entity\LogType;

/**
 * Record template controller.
 */
class RecordTemplateController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The index of record templates.
   *
   * @return array
   *   Returns a render array.
   */
  public function index(): array {

    $items = [
      [
        'title' => $this->t('Asset templates'),
        'description' => $this->t('Templates for creating Assets'),
        'url' => Url::fromRoute('farm_template.templates.asset'),
      ],
      [
        'title' => $this->t('Log templates'),
        'description' => $this->t('Templates for creating Logs'),
        'url' => Url::fromRoute('farm_template.templates.log'),
      ],
    ];

    return [
      '#theme' => 'admin_block_content',
      '#content' => $items,
    ];
  }

  /**
   * The index of asset record templates.
   *
   * @return array
   *   Returns a render array.
   */
  public function asset(AssetType $asset_type = NULL): array {

    // Start cacheability object with template config entity list tag.
    $template_storage = $this->entityTypeManager()->getStorage('farm_record_template');
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheTags($template_storage->getEntityType()->getListCacheTags());

    // Build list item for each quick form.
    $template_query = $template_storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', TRUE)
      ->condition('targetEntityType', 'asset')
      ->sort('bundle');

    if ($asset_type) {
      $template_query->condition('bundle', $asset_type->id());
    }

    $template_ids = $template_query->execute();
    $templates = $template_storage->loadMultiple($template_ids);

    $items = [];
    foreach ($templates as $template) {
      $cacheability->addCacheableDependency($template);
      $url = Url::fromRoute('farm_template.template_form', ['farm_record_template' => $template->id()]);
      if ($url->access()) {
        $items[] = [
          'title' => Markup::create(Html::escape($template->label())),
          'description' => Html::escape($template->getDescription()),
          'url' => $url,
        ];
      }
    }

    // Render items.
    if (!empty($items)) {
      $output = [
        '#theme' => 'admin_block_content',
        '#content' => $items,
      ];
    }
    else {
      $output = [
        '#markup' => $this->t('You do not have any asset templates.'),
      ];
    }
    $cacheability->applyTo($output);
    return $output;
  }


  /**
   * The index of log record templates.
   *
   * @return array
   *   Returns a render array.
   */
  public function log(LogType $log_type = NULL): array {

    // Start cacheability object with template config entity list tag.
    $template_storage = $this->entityTypeManager()->getStorage('farm_record_template');
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheTags($template_storage->getEntityType()->getListCacheTags());

    // Build list item for each quick form.
    $template_query = $template_storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', TRUE)
      ->condition('targetEntityType', 'log')
      ->sort('bundle');
    if ($log_type) {
      $template_query->condition('bundle', $log_type->id());
    }

    $items = [];
    $template_ids = $template_query->execute();
    $templates = $template_storage->loadMultiple($template_ids);
    foreach ($templates as $template) {
      $cacheability->addCacheableDependency($template);
      $url = Url::fromRoute('farm_template.template_form', ['farm_record_template' => $template->id()]);
      if ($url->access()) {
        $items[] = [
          'title' => Markup::create(Html::escape($template->label())),
          'description' => Html::escape($template->getDescription()),
          'url' => $url,
        ];
      }
    }

    // Render items.
    if (!empty($items)) {
      $output = [
        '#theme' => 'admin_block_content',
        '#content' => $items,
      ];
    }
    else {
      $output = [
        '#markup' => $this->t('You do not have any log templates.'),
      ];
    }
    $cacheability->applyTo($output);
    return $output;
  }

}
