<?php

namespace Drupal\farm_template;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides an interface for the template widget plugin manager.
 */
interface TemplateWidgetManagerInterface extends PluginManagerInterface {

  /**
   * Returns field widget options for a given field definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition to load widget options for.
   *
   * @return string[]
   *   Array of field widget options.
   */
  public function getFieldWidgetOptions(FieldDefinitionInterface $field_definition);

}
