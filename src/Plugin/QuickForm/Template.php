<?php

namespace Drupal\farm_template\Plugin\QuickForm;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\farm_quick\Plugin\QuickForm\ConfigurableQuickFormInterface;
use Drupal\farm_quick\Plugin\QuickForm\QuickFormBase;
use Drupal\farm_quick\Traits\ConfigurableQuickFormTrait;
use Drupal\farm_quick\Traits\QuickFormElementsTrait;
use Drupal\farm_quick\Traits\QuickLogTrait;
use Drupal\farm_quick\Traits\QuickTermTrait;
use Drupal\farm_template\Entity\RecordTemplate;
use Psr\Container\ContainerInterface;

/**
 * Inventory quick form.
 *
 * @QuickForm(
 *   id = "record_template",
 *   label = @Translation("Record Template"),
 *   description = @Translation("Create quick forms from record templates."),
 *   permissions = {
 *     "use farm_record_template",
 *   },
 *   requiresEntity = TRUE,
 * )
 */
class Template extends QuickFormBase implements ConfigurableQuickFormInterface {

  use ConfigurableQuickFormTrait;
  use QuickLogTrait;
  use QuickFormElementsTrait;
  use QuickTermTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a QuickFormBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $messenger);
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'template_id' => NULL,
      'default_values' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $template = NULL;
    if (!empty($this->configuration['template_id'])) {
      $template = RecordTemplate::load($this->configuration['template_id']);
    }
    $form['template'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Template'),
      '#target_type' => 'farm_record_template',
      '#default_value' => $template,
      '#required' => TRUE,
      '#disabled' => !empty($template),
    ];

    $form['default_values_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Default values'),
      '#attributes' => [
        'id' => 'default-values-wrapper',
      ],
    ];

    if (!$template) {
      $form['default_values_wrapper']['message'] = [
        '#markup' => $this->t('Select a template and save the quick form to configure default values for the template.'),
      ];
      return $form;
    }

    // Get default values from form state or quick form.
    $default_values = $form_state->get('default_values', []);
    if (!$form_state->has('default_values')) {
      $default_values = $this->configuration['default_values'] ?? [];
    }
    $form_state->set('default_values', $default_values);

    // Index fields and build field options.
    $fields = [];
    foreach ($template->get('fields') ?? [] as $field) {
      $fields[$field['id']] = $field;
      if (!isset($default_values[$field['id']])) {
        $field_options[$field['id']] = $field['label'];
      }
    }

    // Build the field widget.
    /** @var \Drupal\farm_template\TemplateWidgetManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.farm_template_widget');
    $field_definitions = $template->fieldDefinitions();

    // Create default entity. This is needed for building field widgets.
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_storage = $entity_type_manager->getStorage($template->getTargetEntityTypeId());
    $default_entity = $entity_storage->create(['type' => $template->getTargetBundle()]);

    // Build table of default values.
    $form['default_values_wrapper']['default_values'] = [
      '#type' => 'table',
      '#title' => $this->t('Default values'),
      '#header' => ['Field name', 'Default value', 'Required', 'Hidden', 'Actions'],
      '#empty' => $this->t('No fields.'),
      '#tree' => TRUE,
    ];
    foreach ($default_values as $field_id => $default_value) {

      // Skip if the template doesn't have a definition for the field id.
      // This may happen if the template is modified after the quick form.
      if (!isset($fields[$field_id])) {
        continue;
      }
      $default_template_field = $fields[$field_id];

      // Label.
      $form['default_values_wrapper']['default_values'][$field_id]['label'] = [
        '#markup' => $fields[$field_id]['label'],
      ];

      // Default value element.
      $default_template_field['widget_settings']['default_value'] = $default_value['default_value'];
      $plugin_instance = $manager->createInstance($default_template_field['widget'], $default_template_field);
      $field_definition = $field_definitions[$default_template_field['field_name']];
      $form['default_values_wrapper']['default_values'][$field_id]['default_value'] = $plugin_instance->defaultValueElement($default_template_field, $field_definition, $default_entity);

      // Required.
      $form['default_values_wrapper']['default_values'][$field_id]['required'] = [
        '#type' => 'checkbox',
        '#default_value' => $default_value['required'] ?? FALSE,
      ];

      // Hidden.
      $form['default_values_wrapper']['default_values'][$field_id]['hidden'] = [
        '#type' => 'checkbox',
        '#default_value' => $default_value['hidden'] ?? FALSE,
      ];

      // Actions.
      $form['default_values_wrapper']['default_values'][$field_id]['actions'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => [[$this, 'removeField']],
        '#ajax' => [
          'callback' => [$this, 'defaultValuesCallback'],
          'wrapper' => 'default-values-wrapper',
        ],
        '#attributes' => [
          'class' => ['button--danger', 'button--small'],
        ],
      ];
    }

    // Button to add fields.
    $form['default_values_wrapper']['new_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field name'),
      '#options' => $field_options,
    ];
    $form['default_values_wrapper']['add_field'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add field'),
      '#submit' => [[$this, 'addField']],
      '#ajax' => [
        'callback' => [$this, 'defaultValuesCallback'],
        'wrapper' => 'default-values-wrapper',
      ],
      '#attributes' => [
        'class' => ['button--small'],
      ],
    ];

    return $form;
  }

  /**
   * Add field submit callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addField(array &$form, FormStateInterface $form_state) {

    // Make sure the template is available.
    $template = NULL;
    if (!empty($this->configuration['template_id'])) {
      $template = RecordTemplate::load($this->configuration['template_id']);
    }
    if (empty($template)) {
      return;
    }

    // Get the index of the field to remove.
    $field_name = $form_state->getValue(['settings', 'default_values_wrapper', 'new_field']);

    // Index fields and build field options.
    $fields = [];
    foreach ($template->get('fields') ?? [] as $field) {
      $fields[$field['id']] = $field;
    }

    // Add the field.
    $default_values = $form_state->get('default_values') ?? [];
    $default_values[$field_name] = [
      'default_value' => $fields[$field_name]['widget_settings']['default_value'],
      'required' => $fields[$field_name]['required'] ?? FALSE,
      'hidden' => $fields[$field_name]['hidden'] ?? FALSE,
    ];
    $form_state->set('default_values', $default_values);

    // Rebuild form.
    $form_state->setRebuild(TRUE);
  }

  /**
   * Remove field submit callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function removeField(array &$form, FormStateInterface $form_state) {

    // Get the index of the field to remove.
    $triggering = $form_state->getTriggeringElement();
    $field_index = $triggering['#parents'][3];

    // Remove the field.
    $default_values = $form_state->get('default_values') ?? [];
    if (isset($default_values[$field_index])) {
      unset($default_values[$field_index]);
      $form_state->set('default_values', $default_values);
    }

    // Rebuild form.
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for default values.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated fields render array.
   */
  public function defaultValuesCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['default_values_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['template_id'] = $form_state->getValue('template');
    $this->configuration['default_values'] = $form_state->getValue(['default_values_wrapper', 'default_values']) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $template = RecordTemplate::load($this->configuration['template_id']);
    $form['template'] = [
      '#type' => 'farm_template',
      '#template' => $template,
      '#overrides' => $this->configuration['default_values'] ?? [],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (($entity = $form_state->getValue('template')) && ($entity instanceof ContentEntityInterface)) {

      // Save the entity.
      $entity->set('quick', $this->getQuickId());
      $entity->save();

      // Display a message.
      $message = $this->t(
        '@entity_type created: <a href=":url">@label</a>',
        [
          '@entity_type' => $entity->getEntityType()->getLabel(),
          ':url' => $entity->toUrl()->toString(),
          '@label' => $entity->label(),
        ]
      );
      $this->messenger()->addStatus($message);
    }
  }

}
