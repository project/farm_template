<?php

namespace Drupal\farm_template\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'farm_template_default_item' field type.
 *
 * @FieldType(
 *   id = "farm_template_default_item",
 *   label = @Translation("Template default"),
 *   description = @Translation("Stores configuration for a template default."),
 *   default_widget = "farm_template_default",
 * )
 */
class TemplateDefaultItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['field_id'] = DataDefinition::create('string')
      ->setLabel(t('Field Name'));
    $properties['required'] = DataDefinition::create('boolean')
      ->setLabel(t('Required'));
    $properties['hidden'] = DataDefinition::create('boolean')
      ->setLabel(t('Hidden'));
    $properties['default_value'] = DataDefinition::create('any')
      ->setLabel(t('Default Value'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'field_id' => [
          'description' => 'The template field id.',
          'type' => 'varchar_ascii',
          'length' => 255,
          'not null' => TRUE,
        ],
        'required' => [
          'description' => 'The template field is required.',
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
        ],
        'hidden' => [
          'description' => 'The template field is hidden.',
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
        ],
        'default_value' => [
          'description' => 'The template field default value.',
          'type' => 'blob',
          'not null' => TRUE,
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'field_id';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('field_id')->getValue();
    return $value === NULL || $value === '';
  }

}
