<?php

namespace Drupal\farm_template\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the template default widget.
 *
 * @FieldWidget(
 *   id = "farm_template_default",
 *   label = @Translation("Farm Template Default"),
 *   field_types = {
 *     "farm_template_default_item"
 *   }
 * )
 */
class TemplateDefaultWidget extends WidgetBase {

  /**
   * @inheritDoc
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    /** @var \Drupal\farm_template\Plugin\Field\FieldType\TemplateDefaultItem $item */
    $item = $items[$delta];

    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $template = $items->getParent()?->getValue()->get('template')?->first()->entity;
    if (!$template) {
      return;
    }

    // Build field options.
    // @TODO prevent duplicates?
    $field_options = [];
    $fields = [];
    foreach ($template->get('fields') ?? [] as $field) {
      $fields[$field['id']] = $field;
      $field_options[$field['id']] = $field['label'];
    }

    // Bail if there are no field options.
    // @TODO add message with link to template.
    if (empty($field_options)) {
      return;
    }

    // Build ID for ajax replacement.
    $id_prefix = implode('-', array_merge($form['#parents'], [$this->fieldDefinition->getName()]));
    $fieldset_wrapper = Html::getId("$id_prefix-$delta-wrapper");

    // Get defaults from form state or template.
    // @TODO fix populating defaults on AJAX.
    $keys = array_keys($field_options);
    $field_id = $form_state->getValue([$this->fieldDefinition->getName(), $delta, 'field_id'], $item->field_id) ?? reset($keys);
    $default_template_field = $fields[$field_id];

    // Field ID.
    $element['field_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Template field id'),
      '#options' => $field_options,
      '#default_value' => $field_id,
      '#disabled' => !$item->isEmpty(),
      '#ajax' => [
        'callback' => [$this, 'fieldAjaxCallback'],
        'wrapper' => $fieldset_wrapper,
      ],
    ];

    $element['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required'),
      '#default_value' => (bool) ($item->required ?? $default_template_field['required'] ?? FALSE),
    ];

    $element['hidden'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hidden'),
      '#default_value' => (bool) ($item->hidden ?? $default_template_field['hidden'] ?? FALSE),
    ];

    // Prepare the template field definition for the form.
    // This will be used to allow the user to select a default value.
    $default_template_field['label'] = 'Default value';
    $default_template_field['required'] = FALSE;
    $default_template_field['widget_settings']['default_value'] = $item->default_value ?? $default_template_field['widget_settings']['default_value'] ?? NULL;

    // Create default entity. This is needed for building field widgets.
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_storage = $entity_type_manager->getStorage($template->getTargetEntityTypeId());
    $default_entity = $entity_storage->create(['type' => $template->getTargetBundle()]);

    // Build the field widget.
    /** @var \Drupal\farm_template\TemplateWidgetManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.farm_template_widget');
    $plugin_instance = $manager->createInstance($default_template_field['widget'], $default_template_field);
    $field_definitions = $template->fieldDefinitions();
    $field_definition = $field_definitions[$default_template_field['field_name']];
    $element['field_value'] = $plugin_instance->defaultValueElement($default_template_field, $field_definition, $default_entity);
    $element['field_value']['#wrapper_attributes'] = ['id' => $fieldset_wrapper];

    // Wrap this in a fieldset.
    $element += [
      '#type' => 'fieldset',
    ];
    return $element;
  }

  /**
   * Executes the AJAX callback for the field.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array|null
   *   The AJAX response form element array or NULL.
   */
  public function fieldAjaxCallback(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#parents'] ?? NULL;
    if (isset($parents[1])) {
      $delta = $parents[1];
      return $form[$this->fieldDefinition->getName()]['widget'][$delta]['wrapper'];
    }
  }

}
