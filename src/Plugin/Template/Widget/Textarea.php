<?php

namespace Drupal\farm_template\Plugin\Template\Widget;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\farm_template\Attribute\TemplateWidget;

#[TemplateWidget(
  id: "textarea",
  label: new TranslatableMarkup('Text area'),
)]
class Textarea extends TemplateWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function supportsFieldDefinition(FieldDefinitionInterface $field_definition): bool {
    return in_array($field_definition->getType(), ['text_long', 'geofield']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'widget_settings' => [
        'default_value' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['default_value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Default value'),
      '#default_value' => $this->getWidgetSetting('default_value'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $template_field, FieldDefinitionInterface $field_definition, EntityInterface $default_entity) {
    return [
      '#type' => 'textarea',
      '#title' => $template_field['label'],
      '#description' => $template_field['description'],
      '#required' => $template_field['required'],
      '#default_value' => $this->getWidgetSetting('default_value'),
    ];
  }


}
