<?php

namespace Drupal\farm_template\Plugin\Template\Widget;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\farm_template\Attribute\TemplateWidget;

#[TemplateWidget(
  id: "select",
  label: new TranslatableMarkup('Select'),
)]
class Select extends TemplateWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function supportsFieldDefinition(FieldDefinitionInterface $field_definition): bool {
    return in_array($field_definition->getType(), ['entity_reference', 'list_string', 'state']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'widget_settings' => [
        'default_value' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Get field options.
    $options = [];
    $form_state = $form_state->getCompleteFormState();
    /** @var FieldDefinitionInterface $field_definition */
    $field_definition = $form_state->get('field_definition');
    /** @var EntityInterface $default_entity */
    $default_entity = $form_state->get('default_entity');
    if ($options_provider = $field_definition->getOptionsProvider($field_definition->getMainPropertyName(), $default_entity)) {
      $options = $options_provider->getPossibleOptions(\Drupal::currentUser());
    }

    $form['default_value'] =[
      '#type' => 'select',
      '#title' => $this->t('Default value'),
      '#default_value' => $this->getWidgetSetting('default_value'),
      '#options' => $options,
      '#empty_option' => '',
      '#empty_value' => NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $template_field, FieldDefinitionInterface $field_definition, EntityInterface $default_entity) {

    // Get field options.
    $options = [];
    if ($options_provider = $field_definition->getOptionsProvider($field_definition->getMainPropertyName(), $default_entity)) {
      $options = $options_provider->getPossibleOptions(\Drupal::currentUser());
    }

    return [
      '#type' => 'select',
      '#title' => $template_field['label'],
      '#description' => $template_field['description'],
      '#required' => $template_field['required'],
      '#options' => $options,
      '#default_value' => $this->getWidgetSetting('default_value'),
      '#empty_option' => '',
      '#empty_value' => NULL,
    ];
  }

}
