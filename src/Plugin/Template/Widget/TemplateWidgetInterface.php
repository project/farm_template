<?php

namespace Drupal\farm_template\Plugin\Template\Widget;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface for template widget plugins.
 */
interface TemplateWidgetInterface extends PluginFormInterface, ConfigurableInterface, PluginInspectionInterface, CacheableDependencyInterface {

  /**
   * Function to determine if a widget supports a field definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return bool
   *   Boolean if the widget supports a field definition.
   */
  public function supportsFieldDefinition(FieldDefinitionInterface $field_definition): bool ;

  public function render(array $template_field, FieldDefinitionInterface $field_definition, EntityInterface $default_entity);

}
