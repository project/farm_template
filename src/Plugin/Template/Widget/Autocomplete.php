<?php

namespace Drupal\farm_template\Plugin\Template\Widget;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\farm_template\Attribute\TemplateWidget;

#[TemplateWidget(
  id: "autocomplete",
  label: new TranslatableMarkup('Autocomplete'),
)]
class Autocomplete extends TemplateWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function supportsFieldDefinition(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getType() === 'entity_reference';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'widget_settings' => [
        'default_value' => [],
        'multiple' => TRUE,
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if ($form_state instanceof SubformStateInterface) {
      $form_state = $form_state->getCompleteFormState();
    }

    /** @var FieldDefinitionInterface $field_definition */
    $field_definition = $form_state->get('field_definition');

    $form['multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Multiple'),
      '#description' => $this->t('Allow referencing multiple records for this template field.'),
      '#default_value' => $this->configuration['widget_settings']['multiple'] ?? FALSE,
    ];

    // Load default value.
    $default_value = NULL;
    if ($entity_ids = (array) $this->getWidgetSetting('default_value') ?? []) {
      if (count($entity_ids)) {
        $target_type = $field_definition->getSetting('target_type');
        $default_value = \Drupal::entityTypeManager()->getStorage($target_type)->loadMultiple($entity_ids);
      }
    }

    $form['default_value'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Default value'),
      '#tags' => TRUE,
      '#default_value' => $default_value,
      '#target_type' => $field_definition->getSetting('target_type'),
      '#selection_handler' => $field_definition->getSetting('handler'),
      '#selection_settings' => $field_definition->getSetting('handler_settings'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $default = $form_state->getValue(['widget_settings', 'default_value']) ?? [];
    if (is_array($default)) {
      $default = array_column($default, 'target_id');
    }
    else {
      $default = [$default];
    }
    $this->configuration['widget_settings']['default_value'] = $default;
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $template_field, FieldDefinitionInterface $field_definition, EntityInterface $default_entity) {

    // Load default value.
    $default_value = [];
    if ($entity_ids = (array) $this->getWidgetSetting('default_value') ?? []) {
      if (count($entity_ids)) {
        if (isset($entity_ids[0]['target_id'])) {
          $entity_ids = array_column($entity_ids, 'target_id');
        }
        $target_type = $field_definition->getSetting('target_type');
        $default_value = \Drupal::entityTypeManager()->getStorage($target_type)->loadMultiple($entity_ids);
      }
    }

    // Set tags.
    $tags = count($default_value) > 1 || $this->getWidgetSetting('multiple') ?? FALSE;
    return [
      '#type' => 'entity_autocomplete',
      '#title' => $template_field['label'],
      '#description' => $template_field['description'],
      '#tags' => $tags,
      '#required' => $template_field['required'],
      '#default_value' => $tags ? $default_value : reset($default_value),
      '#target_type' => $field_definition->getSetting('target_type'),
      '#selection_handler' => $field_definition->getSetting('handler'),
      '#selection_settings' => $field_definition->getSetting('handler_settings'),
    ];
  }

}
