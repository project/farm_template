<?php

namespace Drupal\farm_template\Plugin\Template\Widget;

use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for template widget plugins.
 */
abstract class TemplateWidgetBase extends PluginBase implements TemplateWidgetInterface {

  use CacheableDependencyTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if ($form_state instanceof SubformStateInterface) {
      $form_state = $form_state->getCompleteFormState();
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Iterate over the plugin configuration and overwrite with submitted form values.
    foreach ($this->getConfiguration() as $key => $value) {
      $this->configuration[$key] = $form_state->getValue($key);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'widget' => $this->getPluginId(),
    ] + $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
    return $this;
  }

  /**
   * Helper function to get a setting from the widget_settings configuration.
   *
   * @param string $setting_name
   *   The key representing the settings name.
   *
   * @return mixed
   *   The settings value.
   */
  protected function getWidgetSetting(string $setting_name): mixed {
    return $this->configuration['widget_settings'][$setting_name] ?? NULL;
  }

  public function defaultValueElement(array $template_field, FieldDefinitionInterface $field_definition, EntityInterface $default_entity) {
    return $this->render($template_field, $field_definition, $default_entity);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
