<?php

namespace Drupal\farm_template\Plugin\Template\Widget;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\farm_template\Attribute\TemplateWidget;

#[TemplateWidget(
  id: "quantity",
  label: new TranslatableMarkup('Quantity'),
)]
class Quantity extends TemplateWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function supportsFieldDefinition(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getType() === 'entity_reference_revisions'
      && $field_definition->getSetting('target_type') === 'quantity';
  }

  public function render(array $template_field, FieldDefinitionInterface $field_definition, EntityInterface $default_entity) {

    // Extract default value.
    // @TODO Extract default values for more than only the quantity value.
    $value_default = NULL;
    $default_values = $this->getWidgetSetting('default_value') ?? NULL;
    if (is_array($default_values) && isset($default_values['value'])) {
      $value_default = $default_values['value'];
    }
    elseif (is_numeric($default_values)) {
      $value_default = $default_values;
    }

    // Build quantity element.
    $settings = $template_field['widget_settings'] ?? [];
    $form = [
      '#type' => 'template_quantity',
      '#title' => $template_field['label'],
      '#description' => $template_field['description'],
      '#required' => $template_field['required'],
      '#value_default' => $value_default,
    ];

    // Add each quantity field as configured.
    foreach (['label', 'measure', 'units'] as $quantity_field) {

      // Check for default value.
      if (isset($settings["{$quantity_field}_value"]) && !empty($settings["{$quantity_field}_value"])) {
        $form["#{$quantity_field}_value"] = $settings["{$quantity_field}_value"];
      }

      // Check for hidden.
      if (isset($settings["{$quantity_field}_hidden"]) && $settings["{$quantity_field}_hidden"]) {
        $form["#{$quantity_field}_hidden"] = TRUE;
      }
    }
    return $form;
  }

  public function defaultValueElement(array $template_field, FieldDefinitionInterface $field_definition, EntityInterface $default_entity) {
    $element = $this->render($template_field, $field_definition, $default_entity);
    $element['#submit_entity'] = FALSE;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Quantity label.
    $form['label_value'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Quantity label'),
      '#default_value' => $this->getWidgetSetting('label_value'),
    ];
    $form['label_hidden'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Hide label'),
      '#default_value' => $this->getWidgetSetting('label_hidden') ?? FALSE,
    ];

    // Quantity measure.
    $form['measure_value'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Quantity measure'),
      '#options' => quantity_measure_options(),
      '#default_value' => $this->getWidgetSetting('measure_value'),
      '#empty_option' => '',
    ];
    $form['measure_hidden'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Hide measure'),
      '#default_value' => $this->getWidgetSetting('measure_hidden') ?? FALSE,
    ];

    // Quantity units.
    $form['units_value'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Quantity units'),
      '#default_value' => $this->getWidgetSetting('units_value'),
    ];
    $form['units_hidden'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Hide units'),
      '#default_value' => $this->getWidgetSetting('units_hidden') ?? FALSE,
    ];

    return $form;
  }

}
