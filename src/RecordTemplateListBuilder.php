<?php

namespace Drupal\farm_template;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Provides a listing of template entities.
 */
class RecordTemplateListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['enabled'] = $this->t('Enabled');
    $header['label'] = $this->t('Template');
    $header['description'] = $this->t('Description');
    $header['record_type'] = $this->t('Record type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $template = $entity;
    $row['enabled'] = $template->get('status') ? new TranslatableMarkup('Yes') : new TranslatableMarkup('No');
    $row['label'] = $template->label();
    $row['description'] = $template->getDescription();
    $row['record_type'] = $template->getRecordTypeLabel();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    // Manage fields.
    $url = Url::fromRoute('farm_template.template_fields_form', ['farm_record_template' => $entity->id()]);
    if ($url->access(\Drupal::currentUser())) {
      $operations['fields'] = [
        'title' => $this->t('Manage Fields'),
        'weight' => 15,
        'url' => Url::fromRoute('farm_template.template_fields_form', ['farm_record_template' => $entity->id()]),
      ];
    }

    // Template form.
    $url = Url::fromRoute('farm_template.template_form', ['farm_record_template' => $entity->id()]);
    if ($url->access(\Drupal::currentUser())) {
      $operations['form'] = [
        'title' => $this->t('Form'),
        'weight' => 20,
        'url' => Url::fromRoute('farm_template.template_form', ['farm_record_template' => $entity->id()]),
      ];
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('No templates available. <a href=":link">Create a template</a>.', [
      ':link' => Url::fromRoute('entity.farm_record_template.add_form')->toString(),
    ]);
    return $build;
  }

}
