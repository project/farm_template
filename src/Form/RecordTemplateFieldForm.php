<?php

namespace Drupal\farm_template\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Url;
use Drupal\farm_template\Entity\RecordTemplate;

/**
 * Entity template form.
 */
class RecordTemplateFieldForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $field_id = NULL, string $new_field = NULL) {
    $form = parent::buildForm($form, $form_state);
    if (!$field_id && !$new_field) {
      return $form;
    }

    // Save the template to form state.
    /** @var RecordTemplate $template */
    $template = $this->entity;
    $form['#title'] = $template->label();
    $fields = $template->getPluginCollections()['fields'] ?? [];

    // Load field definitions.
    /** @var \Drupal\farm_template\TemplateWidgetManagerInterface $widget_manager */
    $widget_manager = \Drupal::service('plugin.manager.farm_template_widget');
    $field_definitions = $template->fieldDefinitions();

    // Add a new field if specified.
    if ($new_field && $field_definition = $field_definitions[$new_field] ?? NULL) {

      // Get the default widget.
      $widget_options = array_keys($widget_manager->getFieldWidgetOptions($field_definition));
      $widget = reset($widget_options);
      if (!$widget) {
        return $form;
      }

      // Load the field definition and build a label.
      // Check the existing count of template fields for this entity field.
      $existing_field_count = count($template->getTemplateEntityFields($new_field));
      $field_label = $field_definition->getLabel() ?? $new_field;
      $field_id = $new_field;
      if ($existing_field_count > 0) {
        $field_label .= " $existing_field_count";
        $field_id .= "_$existing_field_count";
      }

      // Add the field.
      $new_config = [
        'id' => $field_id,
        'field_name' => $new_field,
        'label' => $field_label,
        'description' => NULL,
        'required' => FALSE,
        'default_value' => NULL,
        'hidden' => FALSE,
        'widget' => $widget,
        'widget_settings' => [],
      ];
      $fields->addInstanceId($fields->count(), $new_config);
      $new_field = TRUE;
    }

    // Loop through fields and build the one we are after.
    /**
     * @var  $index
     * @var \Drupal\farm_template\Plugin\Template\Widget\TemplateWidgetInterface $template_field
     */
    foreach ($fields as $index => $template_field) {

      $configuration = $template_field->getConfiguration();
      if ($field_id != $configuration['id']) {
        continue;
      }

      // Save the field index.
      $form['index'] = [
        '#type' => 'hidden',
        '#value' => $index,
      ];

      // Build a tree of field values.
      $form['fields'] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];

      // Field label.
      $form['fields'][$index]['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $configuration['label'],
      ];

      // Field ID. Important this comes after the field label.
      $form['fields'][$index]['id'] = [
        '#type' => 'machine_name',
        '#title' => $this->t('ID'),
        '#machine_name' => [
          'exists' => [$this, 'fieldIdExists'],
          'source' => ['field', 'label'],
        ],
      ];

      // Only set the default_value if this is not a new field.
      $form['fields'][$index]['id']['#default_value'] = $configuration['id'];
      if (!$new_field) {
        $form['fields'][$index]['id']['#default_value'] = $configuration['id'];
      }

      // Save the field name.
      $form['fields'][$index]['field_name'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Field name'),
        '#value' => $configuration['field_name'],
      ];

      // Field description.
      $form['fields'][$index]['description'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Description'),
        '#default_value' => $configuration['description'],
      ];

      // Field required.
      $form['fields'][$index]['required'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Required'),
        '#default_value' => $configuration['required'],
      ];

      // Field hidden.
      $form['fields'][$index]['hidden'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Hidden'),
        '#description' => $this->t('Fields can be hidden from the template when a default value is provided.'),
        '#default_value' => $configuration['hidden'],
      ];

      // Field widget type.
      $field_definition = $field_definitions[$configuration['field_name']];
      $widget_options = $widget_manager->getFieldWidgetOptions($field_definition);
      $widget_ids = array_keys($widget_options);
      $default_widget = reset($widget_ids);
      $default_field_widget = $template_field->getPluginId();
      $form['fields'][$index]['widget'] = [
        '#type' => 'select',
        '#title' => $this->t('Field Widget'),
        '#description' => $this->t('Choose the field widget to render this field.'),
        '#options' => $widget_options,
        '#default_value' => $default_field_widget,
        '#ajax' => [
          'callback' => '::widgetCallback',
          'wrapper' => 'widget-settings',
        ],
      ];

      // Create default entity. This is needed for building field widgets.
      $entity_type_manager = \Drupal::entityTypeManager();
      $entity_storage = $entity_type_manager->getStorage($template->getTargetEntityTypeId());
      $default_entity = $entity_storage->create(['type' => $template->getTargetBundle()]);

      // Allow the plugin type to provide a subform.
      $selected_widget = $form_state->getValue(['widget']) ?? $template_field->getPluginId();
      /** @var \Drupal\farm_template\Plugin\Template\Widget\TemplateWidgetInterface $plugin_instance */
      $plugin_instance = $widget_manager->createInstance($selected_widget, $configuration);

      // Field widget fieldset.
      $form['fields'][$index]['widget_settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Field settings'),
        '#tree' => TRUE,
        '#attributes' => [
          'id' => 'widget-settings',
        ],
      ];

      // Field widget settings.
      $form_state->set('field_definition', $field_definition);
      $form_state->set('default_entity', $default_entity);
      $subform_state = SubformState::createForSubform($form['fields'][$index], $form, $form_state);
      $form['fields'][$index]['widget_settings'] = $plugin_instance->buildConfigurationForm($form['fields'][$index]['widget_settings'], $subform_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm', '::save'],
    ];
    $fields_form_url = Url::fromRoute('farm_template.template_fields_form', ['farm_record_template' => $this->entity->id()]);
    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $fields_form_url,
      '#attributes' => [
        'class' => ['button', 'button--danger'],
      ],
    ];
    return $actions;
  }

  /**
   * Ajax callback for widget.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated widget wrapper render array.
   */
  public function widgetCallback(array &$form, FormStateInterface $form_state) {
    $index = $form_state->getValue('index');
    return $form['fields'][$index]['widget_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $template = $this->entity;

    $index = $form_state->getValue('index');
    $configuration = $form_state->getValue(['fields', $index]);

    // Create default entity. This is needed for building field widgets.
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_storage = $entity_type_manager->getStorage($template->getTargetEntityTypeId());
    $default_entity = $entity_storage->create(['type' => $template->getTargetBundle()]);

    // Create plugin instance with new widget.
    /** @var \Drupal\farm_template\TemplateWidgetManagerInterface $widget_manager */
    $widget_manager = \Drupal::service('plugin.manager.farm_template_widget');
    /** @var \Drupal\farm_template\Plugin\Template\Widget\TemplateWidgetInterface $plugin_instance */
    $plugin_instance = $widget_manager->createInstance($configuration['widget'], $configuration);

    // Load field definitions.
    $field_definitions = $template->fieldDefinitions();
    $form_state->set('field_definition', $field_definitions[$configuration['field_name']]);
    $form_state->set('default_entity', $default_entity);

    // Allow the plugin to submit.
    $subform_state = SubformState::createForSubform($form['fields'][$index], $form, $form_state);
    $plugin_instance->submitConfigurationForm($form['fields'][$index]['widget_settings'], $subform_state);

    // Save the new or changed field.
    $fields = $template->getPluginCollections()['fields'] ?? [];
    $fields->set($index, $plugin_instance);
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    // Save the template.
    $this->entity->save();
    $this->messenger()->addMessage($this->t('Saved the %label template.', [
      '%label' => $this->entity->label(),
    ]));

    // Redirect to the template fields form.
    $fields_form_url = Url::fromRoute('farm_template.template_fields_form', ['farm_record_template' => $this->entity->id()]);
    $form_state->setRedirectUrl($fields_form_url);
  }

  /**
   * Checks if a given field ID exists in the form's fields.
   *
   * @param string $value
   *   The field ID to check.
   * @param mixed $element
   *   The form element.
   * @param mixed $form_state
   *   The form state.
   *
   * @return bool
   *   True if the field ID exists in the form's fields, false otherwise.
   */
  public function fieldIdExists(string $value, array $element, FormStateInterface $form_state) {
    $template = $this->entity;
    $fields = $template->get('fields') ?? [];
    $ids = array_column($fields, 'id');
    return in_array($value, $ids);
  }

}
