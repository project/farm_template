<?php

namespace Drupal\farm_template\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Entity template form.
 */
class RecordTemplateForm extends EntityForm {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $template = $this->entity;

    $form['targetEntityType'] = [
      '#type' => 'select',
      '#title' => $this->t('Record type'),
      '#description' => $this->t('Choose the type of record to create a template for.'),
      '#options' => [
        'asset' => $this->t('Asset'),
        'log' => $this->t('Log'),
      ],
      '#required' => TRUE,
      '#default_value' => $template->getTargetEntityTypeId(),
      '#disabled' => !$template->isNew(),
      '#ajax' => [
        'callback' => '::bundleCallback',
        'wrapper' => 'bundle-wrapper',
      ],
    ];

    $bundle_options = [];
    $entity_type = $form_state->getValue('targetEntityType') ?? $template->getTargetEntityTypeId();
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);
    $bundle_options = array_map(function (array $bundle) {
      return $bundle['label'];
    }, $bundles);

    $form['bundle'] = [
      '#type' => 'select',
      '#title' => $entity_type . ' type',
      '#options' => $bundle_options,
      '#required' => TRUE,
      '#default_value' => $template->getTargetBundle(),
      '#disabled' => !$template->isNew(),
      '#wrapper_attributes' => [
        'id' => 'bundle-wrapper',
      ],
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $template->label(),
      '#description' => $this->t('Label for the template.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $template->id(),
      '#machine_name' => [
        'exists' => '\Drupal\farm_template\Entity\RecordTemplate::load',
      ],
      '#disabled' => !$template->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $template->getDescription(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $template->status(),
    ];

    return $form;
  }

  /**
   * Ajax callback for bundle.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated fields render array.
   */
  public function bundleCallback(array &$form, FormStateInterface $form_state) {
    return $form['bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntity() {

    // Template should be enabled by default.
    if ($this->entity->isNew()) {
      $this->entity->set('status', TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $template = $this->entity;
    $status = $template->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label template.', [
          '%label' => $template->label(),
        ]));
        $form_state->setRedirectUrl(Url::fromRoute('farm_template.template_fields_form', ['farm_record_template' => $template->id()]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label template.', [
          '%label' => $template->label(),
        ]));
        $form_state->setRedirectUrl($template->toUrl('collection'));
    }

    return $status;
  }

}
