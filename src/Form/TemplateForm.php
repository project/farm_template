<?php

namespace Drupal\farm_template\Form;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\farm_template\Entity\RecordTemplate;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Template form.
 */
class TemplateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_template_form';
  }

  /**
   * Access callback.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   * @param \Drupal\farm_template\Entity\RecordTemplate|null $farm_record_template
   *   The template entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, RecordTemplate $farm_record_template = NULL) {
    if (is_null($farm_record_template)) {
      throw new ResourceNotFoundException();
    }
    $access = AccessResultAllowed::allowedIfHasPermission($account, 'use farm_record_template');
    return $access->andIf(\Drupal::entityTypeManager()->getAccessControlHandler($farm_record_template->getTargetEntityTypeId())
      ->createAccess($farm_record_template->getTargetBundle(), $account, [], TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, RecordTemplate $farm_record_template = NULL) {

    $template = $farm_record_template;
    $form_state->set('template', $template);

    $form['#title'] = $template->label();

    $form['template'] = [
      '#type' => 'farm_template',
      '#template' => $template,
      '#debug' => (bool) $this->getRequest()->get('debug', FALSE),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if (($entity = $form_state->getValue('template')) && ($entity instanceof ContentEntityInterface)) {

      // Save the entity.
      $entity->save();

      // Display a message.
      $message = $this->t(
        '@entity_type created: <a href=":url">@label</a>',
        [
          '@entity_type' => $entity->getEntityType()->getLabel(),
          ':url' => $entity->toUrl()->toString(),
          '@label' => $entity->label(),
        ]
      );
      $this->messenger()->addStatus($message);
    }
  }

}
