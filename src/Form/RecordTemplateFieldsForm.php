<?php

namespace Drupal\farm_template\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Entity template form.
 */
class RecordTemplateFieldsForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $template = $this->entity;
    $form['#title'] = $template->label();

    // Filter field definitions to those that have supported types.
    /** @var \Drupal\farm_template\TemplateWidgetManagerInterface $widget_manager */
    $widget_manager = \Drupal::service('plugin.manager.farm_template_widget');
    $field_definitions = $template->fieldDefinitions();
    $filtered_field_definitions = array_filter($field_definitions, function (FieldDefinitionInterface $field_definition) use ($widget_manager) {
      return count($widget_manager->getFieldWidgetOptions($field_definition)) > 0;
    });

    // Build field definition options array.
    $field_definition_options = array_map(function (FieldDefinitionInterface $field_definition) {
      return $field_definition->getLabel();
    }, $filtered_field_definitions);

    // Get fields from form state or template.
    $fields = $form_state->get('fields', []);
    if (!$form_state->has('fields')) {
      $fields = $template->get('fields') ?? [];
    }
    $form_state->set('fields', $fields);

    // Fields wrapper.
    $form['fields_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'fields-wrapper',
      ],
    ];

    // Build table.
    $table = [
      '#type' => 'table',
      '#title' => $this->t('Template Fields'),
      '#header' => ['Label', 'Weight', 'Description', 'Required', 'Hidden', 'Actions'],
      '#empty' => $this->t('No fields.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'fields-table',
      ],
    ];

    // Add a table row for each field.
    foreach ($fields as $index => $template_field) {

      // Build field label/summary.
      $field_id = $template_field['id'];
      $field_name = $template_field['field_name'];
      $field_label = $template_field['label'];
      $field_definition = $filtered_field_definitions[$field_name];
      $real_field_label = $field_definition->getLabel() ?? $field_name;
      $summary = $field_label;
      if ($field_label != $real_field_label || $field_id != $field_name) {
        $summary .= " ($real_field_label - $field_id)";
      }

      // Add draggable row to the table.
      $table[$index] = [
        '#attributes' => [
          'class' => ['draggable', 'tabledrag-leaf'],
        ],
        '#weight' => $index,
      ];

      // Add field label.
      $table[$index]['label'] = [
        '#markup' => $summary,
      ];

      // Add field weight.
      // @todo This cannot be the first column in the table else drag breaks.
      $table[$index]['weight'] = [
        '#type' => 'weight',
        '#title' => 'Weight',
        '#default_value' => $index,
        '#title_display' => 'invisible',
        '#attributes' => [
          'class' => ['table-sort-weight'],
        ],
      ];

      // Field description.
      $table[$index]['description'] = [
        '#markup' => $template_field['description'],
      ];

      // Field required.
      $table[$index]['required'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Required'),
        '#default_value' => $template_field['required'],
        '#disabled' => TRUE,
      ];

      // Field hidden.
      $table[$index]['hidden'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Hidden'),
        '#default_value' => $template_field['hidden'],
        '#disabled' => TRUE,
      ];

      // Actions for the field.
      $url = Url::fromRoute('farm_template.template_field_form', ['farm_record_template' => $template->id(), 'field_id' => $field_id]);
      $table[$index]['actions']['edit_field'] = [
        '#type' => 'link',
        '#title' => $this->t('Edit'),
        '#url' => $url,
        '#attributes' => [
          'class' => ['button', 'button--small'],
        ],
        '#ajax' => [
          'dialogType' => 'modal',
          'dialog' => ['width' => 'auto'],
        ],
      ];
      $table[$index]['actions']['remove_field'] = [
        '#type' => 'submit',
        '#name' => "remove-field-$index",
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeField'],
        '#ajax' => [
          'callback' => '::fieldsCallback',
          'wrapper' => 'fields-wrapper',
        ],
        '#attributes' => [
          'id' => "remove-field-$index",
          'class' => ['button--danger', 'button--small'],
        ],
      ];
    }

    // Add table to the form.
    $form['fields_wrapper']['fields'] = $table;

    // Add field options.
    $form['add_field'] = [
      '#type' => 'select',
      '#options' => $field_definition_options,
      '#ajax' => [
        'callback' => '::addFieldButtonCallback',
        'wrapper' => 'add-field-wrapper',
      ],
    ];

    // Render link to add fields. Use ajax to replace the new field name.
    $field_options = array_keys($field_definition_options);
    $selected_field_type = $form_state->getValue('add_field', reset($field_options));
    $url = Url::fromRoute('farm_template.template_add_field_form', ['farm_record_template' => $template->id(), 'new_field' => $selected_field_type]);
    $form['add_new_field'] = [
      '#type' => 'link',
      '#title' => $this->t('Add field'),
      '#url' => $url,
      '#attributes' => [
        'class' => ['button', 'button--small'],
        'id' => 'add-field-wrapper',
      ],
      '#ajax' => [
        'dialogType' => 'modal',
        'dialog' => ['width' => 'auto'],
      ],
    ];
    return $form;
  }

  /**
   * Remove field submit callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function removeField(array &$form, FormStateInterface $form_state) {

    // Get the index of the field to remove.
    $triggering = $form_state->getTriggeringElement();
    $field_index = $triggering['#parents'][1];

    // Remove the field.
    $fields = $form_state->get('fields') ?? [];
    if (isset($fields[$field_index])) {
      $field_label = $fields[$field_index]['label'];
      unset($fields[$field_index]);
      $form_state->set('fields', $fields);
      $this->messenger()->addStatus($this->t('Removed field: %field', ['%field' => $field_label]));
    }

    // Rebuild form.
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for fields.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated fields render array.
   */
  public function fieldsCallback(array &$form, FormStateInterface $form_state) {
    return $form['fields_wrapper'];
  }

  /**
   * Ajax callback for add new field button.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated fields render array.
   */
  public function addFieldButtonCallback(array &$form, FormStateInterface $form_state) {
    return $form['add_new_field'];
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);

    // Sort fields before copying to template entity.
    $new_fields = $order = [];

    // Make an array with the weights.
    $submitted_fields = $form_state->getValue('fields') ?? [];
    foreach ($submitted_fields as $field => $info) {
      if (is_array($info) && isset($info['weight'])) {
        $order[$field] = $info['weight'];
      }
    }

    // Sort the array.
    asort($order);

    // Create a list of fields in the new order.
    $template_fields = $form_state->get('fields') ?? [];
    foreach (array_keys($order) as $field) {
      if (isset($template_fields[$field])) {
        $new_fields[$field] = $template_fields[$field];
      }
    }

    // Set fields back to the template entity.
    $entity->set('fields', array_values($new_fields));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    // Save the template.
    $this->entity->save();
    $this->messenger()->addMessage($this->t('Saved the %label template.', [
      '%label' => $this->entity->label(),
    ]));

    // Redirect to the template fields form.
    $fields_form_url = Url::fromRoute('farm_template.template_fields_form', ['farm_record_template' => $this->entity->id()]);
    $form_state->setRedirectUrl($fields_form_url);
  }

}
