<?php

namespace Drupal\farm_template_plan\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\plan\Entity\PlanInterface;
use Drupal\plan\Entity\PlanRecord;
use Drupal\plan\Entity\PlanRecordInterface;

class PlanAddTemplateForm extends FormBase {

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'farm_template_plan_add_template_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, PlanInterface $plan = NULL, PlanRecordInterface $plan_record = NULL) {

    if (empty($plan)) {
      return $form;
    }
    $form_state->set('plan_id', $plan->id());

    $form['template'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Template'),
      '#target_type' => 'farm_record_template',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;

  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plan_id = $form_state->get('plan_id');
    $record = PlanRecord::create([
      'type' => 'template',
      'plan' => $plan_id,
      'template' => $form_state->getValue('template'),
      'order' => 100,
    ]);
    $record->save();
    $form_state->setRedirect('farm_template_plan.templates_form', ['plan' => $plan_id]);
  }

}
