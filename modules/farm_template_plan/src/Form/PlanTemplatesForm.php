<?php

namespace Drupal\farm_template_plan\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\plan\Entity\PlanInterface;
use Drupal\plan\Entity\PlanRecord;

/**
 * Entity template form.
 */
class PlanTemplatesForm extends FormBase {

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'farm_template_plan_add_template_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, PlanInterface $plan = NULL) {

    /** @var \Drupal\farm_template\Entity\RecordTemplate $template */
    $form['#title'] = $plan->label() . ' templates';

    $plan_record_ids = \Drupal::entityTypeManager()->getStorage('plan_record')->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', 'template')
      ->condition('plan', $plan->id())
      ->sort('order')
      ->sort('id')
      ->execute();

    $plan_records = [];
    if (count($plan_record_ids)) {
      $plan_records = \Drupal::entityTypeManager()->getStorage('plan_record')->loadMultiple($plan_record_ids);
    }

    $form_state->set('plan_records', $plan_records);

    // Fields wrapper.
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'wrapper',
      ],
    ];

    // Build table.
    $table = [
      '#type' => 'table',
      '#title' => $this->t('Plan Templates'),
      '#header' => ['Template name', 'Weight', 'Description', 'Record type', 'Actions'],
      '#empty' => $this->t('No defaults.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'defaults-table',
      ],
    ];

    // Add a table row for each field.
    foreach ($plan_records as $id => $plan_record) {

      $template = $plan_record->get('template')?->first()?->entity;
      if (!$template) {
        continue;
      }

      // Add draggable row to the table.
      $weight = $plan_record->get('order')->value;
      $table[$id] = [
        '#attributes' => [
          'class' => ['draggable', 'tabledrag-leaf'],
        ],
        '#weight' => $weight,
      ];

      // Add field label.
      $table[$id]['label'] = [
        '#markup' => $template->label(),
      ];

      // Add field weight.
      // @todo This cannot be the first column in the table else drag breaks.
      $table[$id]['weight'] = [
        '#type' => 'weight',
        '#title' => 'Weight',
        '#default_value' => $weight,
        '#title_display' => 'invisible',
        '#attributes' => [
          'class' => ['table-sort-weight'],
        ],
      ];

      // Field description.
      $table[$id]['description'] = [
        '#markup' => $template->getDescription(),
      ];

      // Record type.
      $table[$id]['record_type'] = [
        '#markup' => $template->getRecordTypeLabel(),
      ];


      // Actions for the field.
      // Use the template.
      $destination = Url::fromRoute('farm_template_plan.templates_form', ['plan' => $plan->id()])->toString();
      $url = Url::fromRoute('farm_template_plan.template_form', ['plan' => $plan->id(), 'farm_record_template' => $template->id()], ['query' => ['destination' => $destination]]);
      $table[$id]['actions']['use'] = [
        '#type' => 'link',
        '#title' => $this->t('Template'),
        '#url' => $url,
        '#attributes' => [
          'class' => ['button', 'button--small', 'button--primary'],
        ],
        '#ajax' => [
          'dialogType' => 'modal',
          'dialog' => ['width' => 'auto'],
        ],
      ];

      // Edit the template.
      $destination = Url::fromRoute('farm_template_plan.templates_form', ['plan' => $plan->id()])->toString();
      $url = Url::fromRoute('entity.plan_record.edit_form', ['plan_record' => $plan_record->id()], ['query' => ['destination' => $destination]]);
      $table[$id]['actions']['edit'] = [
        '#type' => 'link',
        '#title' => $this->t('Edit'),
        '#url' => $url,
        '#attributes' => [
          'class' => ['button', 'button--small'],
        ],
        '#ajax' => [
          'dialogType' => 'modal',
          'dialog' => ['width' => 'auto'],
        ],
      ];
      $table[$id]['actions']['remove'] = [
        '#type' => 'submit',
        '#name' => "remove-$id",
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeTemplate'],
        '#ajax' => [
          'callback' => '::templatesCallback',
          'wrapper' => 'wrapper',
        ],
        '#attributes' => [
          'id' => "remove-$id",
          'class' => ['button--danger', 'button--small'],
        ],
      ];
    }

    // Add table to the form.
    $form['wrapper']['templates'] = $table;

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'class' => ['button', 'button--primary'],
      ],
    ];

    $url = Url::fromRoute('farm_template_plan.add_template_form', ['plan' => $plan->id()]);
    $form['add_template'] = [
      '#type' => 'link',
      '#title' => $this->t('Add template'),
      '#url' => $url,
      '#attributes' => [
        'class' => ['button', 'button--secondary'],
      ],
      '#ajax' => [
        'dialogType' => 'modal',
        'dialog' => ['width' => 'auto'],
      ],
    ];


    return $form;
  }

  /**
   * Remove field submit callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function removeTemplate(array &$form, FormStateInterface $form_state) {

    // Get the index of the field to remove.
    $triggering = $form_state->getTriggeringElement();
    $field_id = $triggering['#parents'][1];

    if ($record = PlanRecord::load($field_id)) {
      $record->delete();
      $this->messenger()->addStatus($this->t('Removed template from plan.'));
    }

    // Rebuild form.
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for fields.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated fields render array.
   */
  public function templatesCallback(array &$form, FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Sort plan_records before saving.
    $order = [];

    // Make an array with the weights.
    $submitted = $form_state->getValue('templates') ?? [];
    foreach ($submitted as $default => $info) {
      if (is_array($info) && isset($info['weight'])) {
        $order[$default] = $info['weight'];
      }
    }

    // Sort the array.
    asort($order);

    // Create a list of fields in the new order.
    $updated = $form_state->get('plan_records') ?? [];
    foreach (array_keys($order) as $field) {
      if (isset($updated[$field])) {
        $updated[$field]->set('order', $order[$field]);
        $updated[$field]->save();
      }
    }

  }

}
