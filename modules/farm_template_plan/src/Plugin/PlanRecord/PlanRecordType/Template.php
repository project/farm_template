<?php

namespace Drupal\farm_template_plan\Plugin\PlanRecord\PlanRecordType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\farm_entity\Plugin\PlanRecord\PlanRecordType\FarmPlanRecordType;

/**
 * Provides the template plan record type.
 *
 * @PlanRecordType(
 *   id = "template",
 *   label = @Translation("Plan Template"),
 * )
 */
class Template extends FarmPlanRecordType {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['template'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Template'))
      ->setDescription($this->t('Associates the plan with a template.'))
      ->setSetting('target_type', 'farm_record_template')
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['order'] = BundleFieldDefinition::create('integer')
      ->setLabel($this->t('Template order'))
      ->setRequired(TRUE)
      ->setDefaultValue(100);

    $fields['template_default'] = BundleFieldDefinition::create('farm_template_default_item')
      ->setLabel($this->t('Template defaults'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'farm_template_default',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
