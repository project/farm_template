<?php

namespace Drupal\farm_template_plan\Plugin\Plan\PlanType;

use Drupal\farm_entity\Plugin\Plan\PlanType\FarmPlanType;

/**
 * Provides the template plan type.
 *
 * @PlanType(
 *   id = "template",
 *   label = @Translation("Template"),
 * )
 */
class Template extends FarmPlanType {

}
