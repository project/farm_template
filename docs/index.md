---
hide:
  - toc
---

# Farm Template: Record templates for farmOS

The Farm Template module provides a new concept of templates for use with
[farmOS](https://farmos.org). Templates make it faster and easier to
create consistent Asset and Log records that follow a standard data convention.

## User Guide

Read the user guide to familiarize yourself with the structure of templates and
learn how to use templates throughout farmOS:

- [Introduction](guide/index.md)
- [Widgets](guide/widgets.md)
- [Menus](guide/menus.md)
- [Quick Forms](guide/quick-forms.md)
- [Plans](guide/plans.md)
- [Exporting](guide/exporting.md)

## Developer Guide

Read the developer guide to learn how to use and extend template functionality:

- [Introduction](development/index.md)
- [Widgets](development/widgets.md)
- [Form Element](development/form-element.md)
- [JSON-API](development/api.md)

