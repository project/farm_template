---
hide:
  - toc
---

# User Guide

Templates provide a flexible way to work with specific record keeping
use-cases in farmOS. At their core templates define a standard data
convention for the fields and values that should be included for any particular
Asset or Log record. Once a template has been created by a user in farmOS the
template can be used in different places throughout farmOS to quickly create
records that follow the standard data convention.

Templates can be created and modified directly form the user-interface without
writing any code.

## Setup

Templates are managed in the Template Setup page of farmOS. From the main menu
navigate to `Setup -> Templates`.

This page will show a list of all templates that have been created in the farmOS
instance. There are links to create new templates and edit or delete existing
templates.

## Concepts

Templates are implemented as configuration entities in farmOS. This allows
modules to provide hard-coded default templates out of the box but also allows
users to further configure and create templates customized for their record
keeping needs. Templates are comprised of three general things: template
metadata, fields and widgets.

### Template metadata

Each template has some simple metadata associated with it:
- `id`: A unique identifier (or "machine name") that identifies the template.
  Best practice is to use an ID that is 1-3 short words. The ID cannot be
  changed after creating the template.
- `name`: A short name for template.
- `description`: A brief description that describes how or when the template
  should be used.
- `record_type` and `record_bundle`: Templates can only be configured for a
  single type of assets or logs. This cannot be changed after creating the
  template.

### Fields

After a template is created with basic metadata you can begin configuring
fields for the record type configured for the template. Field options are
limited to those that exist for the specified record type. For example,
since all assets have a `name` field this can be added to any asset template.
But the `animal_type` field can only be added to templates for animal assets.

All fields have a few options that can be configured:
- `field_id`: A unique ID for this field in the template. This is different
  from the field name because fields can be added multiple times to a template.
- `label`: A label for the field. Defaults to the field label but can be
  changed for more specificity.
- `description`: A brief description for the field that will be shown in forms.
- `required`: If the field is considered required data on a template.
- `hidden`: If the field should be hidden and use a configured default value
  when using the template.
- `widget`: A single widget must be configured for each template field. The
  widget will determine how the field is rendered in forms and provide
  additional options for configuring the field's allowed values and other
  behavior.

Fields can be added to a template multiple times. This makes it easier to
collect similar data via multiple questions rather than all at once in a single
prompt. The resulting behavior for how data is saved to the asset or log
record depends if the field is multivalued:

If a record field is multivalued:
- Each field from the template will be appended to create a list of values
  on the record.
- For example, logs can reference multiple assets, and a template could be
  configured with 3 `asset` fields to prompt a user to select the log's
  assets from 3 distinct, pre-configured groups of assets. The resulting log
  would contain all assets selected from the 3 template fields.

If a record field *is not* multivalued:
- Each field from the template will be combined together to create a single
  value on the record. This makes the most sense for text or string field
  types and may not be supported for all field types.
- For example, the `name` field can be added to a template two times, first
  to configure required and hidden prefix eg: `Activity name: `, and second
  with a required empty text field for the user to complete. The resulting
  record name would be combined to create `Activity name: {user
  input}`.

### Widgets

Field widgets are responsible for rending template fields in forms and
providing additional options for configuring the field's behavior in the
template. Widgets are only applicable to certain types of fields and may
behave differently for different fields.
