---
hide:
  - toc
---

# Menus

Some menus in farmOS will be changed to display quick menus and buttons to
"Create Records" instead of just "Add asset" or "Add log". These menus can
be used to quickly create new records from configured templates instead of
creating records from scratch.


