---
hide:
  - toc
---

# Quick Forms

New Quick Forms can be created to use existing templates. This makes it
easier to access templates and quickly create new records from the existing
Quick Form interface. Navigate to `Setup -> Quick Forms` and create a new
`Record Template` quick form.

## Configurable quick forms

Configurable quick forms provide a second layer of configuration over standard
templates and template forms: each instance of a template quick form
can be configured with different configuration and default values for
each template field. This means that *multiple quick forms can be created
for the same template* but with different configuration and
default values for the template fields.

This might be useful for frequent data collection that is similar but might
have small changes that depend on the asset it is being recorded against.
For example, a template for egg collection could be created with a generic
`asset` field. Separate quick forms can be created for each flock of birds
with the `asset` field pre-configured and hidden in each quick form.
