---
hide:
  - toc
---

# JSON-API

Because templates are implemented as standard Drupal configuration entities
this means they can easily be created and modified via JSON-API. View the
list of templates at `/api/farm_record_template/farm_record_template`.

See the documentation for more info on using the
[farmOS API](https://farmos.org/development/api/)
