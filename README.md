# farm_template
Provides features to create templates for standardized record keeping and
planning in farmOS.

This module is an add-on for the [farmOS](http://drupal.org/project/farm)
distribution.

See the documentation for more information: https://project.pages.drupalcode.org/farm_template/

## Maintainers

Current maintainers:
- Paul Weidner - [@paul121](https://github.com/paul121)

