# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 1.0.0-alpha3 2024-06-21

### Changed

- Move to farmOS Contrib package so template plan module appears in setup page.

## 1.0.0-alpha2 2024-06-21

### Changed

- Move to farmOS Contrib package so module appears in setup page.

## 1.0.0-alpha1 2024-06-21

Initial alpha release to enable user feedback and testing.

Expect there to be breaking changes, please do not use in a production
environment.
